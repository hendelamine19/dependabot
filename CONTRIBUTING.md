# Contributing to dependabot-gitlab

We love your input! We want to make contributing to this project as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features
- Becoming a maintainer

## We Develop with GitLab

We use GitLab to host code, to track issues and feature requests, as well as accept merge requests.

## All Code Changes Happen Through Merge Requests

Merge requests are the best way to propose changes to the codebase. We actively welcome your merge requests:

1. Fork the repo and create your branch from `main`.
2. If you've added code that should be tested, add tests.
3. Ensure pipeline passes.
4. Submit that merge request!

## Any contributions you make will be under the MIT Software License

In short, when you submit code changes, your submissions are understood to be under the same [MIT License](http://choosealicense.com/licenses/mit/) that covers the project. Feel free to contact the maintainers if that's a concern.

## Report bugs using GitLabs's [issues](https://docs.gitlab.com/ee/user/project/issues)

We use GitLab issues to track public bugs. Report a bug by [opening a new issue](https://gitlab.com/dependabot-gitlab/dependabot/-/issues/new); it's that easy!

## Use provided templates when submitting issues

Issues have predefined templates, please use those when submitting a new issues. The more information you provide, the more chance for a certain feature to be implemented or a certain bug to be fixed.

## Development

Tests are executed using `bundle exec rspec` command. Tests are divided into 3 categories:

- `unit` - tests that do not require any external dependencies
- `integration` - tests that require external dependencies (mongodb, redis)
- `system` - tests that require external dependencies and external services (mock server)

### Locally

Following prerequisites are required to run tests locally:

- Up to date Ruby version (see `.ruby-version`)
- Running instance of mongodb (by default on `mongodb://localhost:27017`)
- Running instance of redis (by default on `redis://localhost:6379`)

### Docker Compose

It is possible to start fully functional test environment via `docker compose`. Environment exposes web page on port `3000`. This environment is most useful for running e2e tests.

To start environment, run following command:

```bash
bin/dev-env start
```

To stop environment, run following command:

```bash
bin/dev-env stop
```

### Devcontainer

This is prefered way for running system tests (`bundle exec rspec --tag system`) and E2E tests (`npm run e2e:test`).

[Remote development](https://code.visualstudio.com/docs/devcontainers/containers) is supported via [devcontainer](.devcontainer). It contains all required dependencies to run fully set up environment. To start development, open this repository in VSCode and click on `Reopen in Container` button.

All tests can be executed via `rspec` command. By default, devcontainer is built using `bundle` ecosystem base image, so if trying to run dependency update end-to-end like in a real instance of app, only `gem` updates or updates that do not require native helpers will work.

#### E2E tests

To run E2E tests, web and sidekiq process needs to be started first. This can be done by running following command in devcontainer:

```bash
start_app
```

App logs can be found in `/home/dependabot/log` directory.

## References

This document was adapted from the basic open-source contribution [template](https://gist.github.com/briandk/3d2e8b3ec8daf5a27a62)
