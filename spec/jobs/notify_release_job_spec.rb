# frozen_string_literal: true

describe NotifyReleaseJob, type: :job do
  subject(:job) { described_class }

  let(:dependency_name) { "rspec" }
  let(:package_ecosystem) { "bundler" }
  let(:project_name) { "project" }
  let(:ignore_rules) { false }
  let(:container_runner_class) { Container::Compose::Runner }
  let(:args) { [dependency_name, package_ecosystem, project_name, ignore_rules] }

  before do
    allow(container_runner_class).to receive(:call)
  end

  around do |example|
    with_env("SETTINGS__DEPLOY_MODE" => "compose") { example.run }
  end

  it { is_expected.to be_retryable false }

  it "queues job in low queue" do
    expect { job.perform_later(*args) }.to enqueue_sidekiq_job.on("low")
  end

  it "triggers updates" do
    job.perform_now(*args)

    expect(container_runner_class).to have_received(:call).with(
      package_ecosystem: package_ecosystem,
      task_name: "notify_release",
      task_args: args
    )
  end
end
