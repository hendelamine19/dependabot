# frozen_string_literal: true

describe BackgroundMigrationJob, type: :job do
  subject(:job) { described_class }

  let(:migrations_path) { Rails.root.join("db/migrate/background") }
  let(:migrator) { instance_double(Mongoid::Migrator, migrate: nil) }

  let(:migrations) { [1, 2] }
  let(:migrated) { [1] }

  before do
    allow(Mongoid::Migrator).to receive(:new)
      .with(:up, migrations_path, background_migration: true)
      .and_return(migrator)

    allow(migrator).to receive(:migrations) { migrations }
    allow(migrator).to receive(:migrated) { migrated }
  end

  it { is_expected.to be_retryable false }

  it "queues job in low queue" do
    expect { job.perform_later }.to enqueue_sidekiq_job.on("low")
  end

  context "with pending migrations" do
    it "performs migrations" do
      job.perform_now

      expect(migrator).to have_received(:migrate)
    end
  end

  context "without pending migrations" do
    let(:migrated) { [1, 2] }

    it "skips migrations" do
      job.perform_now

      expect(migrator).not_to have_received(:migrate)
    end
  end
end
