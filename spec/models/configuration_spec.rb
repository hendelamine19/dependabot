# frozen_string_literal: true

describe Configuration, :integration do
  let!(:project) { create(:project, config_yaml: config_yaml) }

  let(:persisted_project) { Project.find_by(name: project.name) }

  let(:docker_username) { "octocat" }
  let(:docker_parsed_username) { docker_username }
  let(:docker_password) { "docker-password" }
  let(:docker_parsed_password) { docker_password }

  let(:npm_token) { "npm-token" }
  let(:npm_parsed_token) { npm_token }

  let(:hex_key) { "key" }
  let(:hex_fingerprint) { "fingerprint" }
  let(:hex_parsed_key) { hex_key }
  let(:hex_parsed_fingerprint) { hex_fingerprint }

  let(:config_yaml) do
    <<~YAML
      version: 2
      registries:
        dockerhub:
          type: docker-registry
          url: registry.hub.docker.com
          username: #{docker_username}
          password: #{docker_password}
        npm:
          type: npm-registry
          url: https://npm.pkg.github.com
          token: #{npm_token}
        hex:
          type: hex-repository
          repo: private-repo
          url: https://private-repo.example.com
          auth-key: #{hex_key}
          public-key-fingerprint: #{hex_fingerprint}
      updates:
        - package-ecosystem: bundler
          directory: "/"
          schedule:
            interval: weekly
    YAML
  end

  let(:registries) do
    [
      {
        "type" => "docker_registry",
        "registry" => "registry.hub.docker.com",
        "username" => "octocat",
        "password" => docker_parsed_password
      },
      {
        "type" => "npm_registry",
        "registry" => "npm.pkg.github.com",
        "token" => npm_parsed_token
      },
      {
        "type" => "hex_repository",
        "repo" => "private-repo",
        "url" => "https://private-repo.example.com",
        "auth_key" => hex_parsed_key,
        "public_key_fingerprint" => hex_parsed_fingerprint
      }
    ]
  end

  describe "#updates", feature: "updates config" do
    it "returns persisted config" do
      expect(
        persisted_project.configuration.entry(package_ecosystem: "bundler").slice(:package_ecosystem, :directory)
      ).to eq(
        package_ecosystem: "bundler",
        directory: "/"
      )
    end
  end

  describe "#registries", feature: "registries config" do
    context "without value from environment variable" do
      it "returns registries credentials" do
        expect(persisted_project.configuration.registries.select(".*").map(&:to_h)).to eq(registries)
      end
    end

    context "with value from environment variable" do
      let(:docker_username) { "${{DOCKERHUB_USERNAME}}" }
      let(:docker_parsed_username) { "octocat" }
      let(:docker_password) { "${{DOCKERHUB_PASSWORD}}" }
      let(:docker_parsed_password) { "docker-password" }

      let(:npm_token) { "${{NPM_TEST_TOKEN}}" }
      let(:npm_parsed_token) { "npm-token" }

      let(:hex_key) { "${{HEX_KEY}}" }
      let(:hex_parsed_key) { "key" }
      let(:hex_fingerprint) { "${{HEX_FINGERPRINT}}" }
      let(:hex_parsed_fingerprint) { "fingerprint" }

      let(:env) do
        {
          "DOCKERHUB_USERNAME" => docker_parsed_username,
          "DOCKERHUB_PASSWORD" => docker_parsed_password,
          "NPM_TEST_TOKEN" => npm_parsed_token,
          "HEX_KEY" => hex_parsed_key,
          "HEX_FINGERPRINT" => hex_parsed_fingerprint
        }
      end

      around do |example|
        with_env(env) { example.run }
      end

      it "returns registries credentials with correct password" do
        expect(persisted_project.configuration.registries.select(".*").map(&:to_h)).to eq(registries)
      end
    end
  end
end
