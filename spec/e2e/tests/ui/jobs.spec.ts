import { expect, test } from "@playwright/test";
import { SignInPage } from "@pages/signIn";
import { ProjectsPage } from "@pages/projects";

import { Smocker } from "@support/mocks/smocker";
import { mocks } from "@support/mocks/mocks";
import { user } from "@support/user";
import { ProjectFabricator } from "@support/fabricators/projectFabricator";

import randomstring from "randomstring";

let projectName: string;
let projectsPage: ProjectsPage;
let smocker: Smocker;

test.beforeEach(async ({ page, request }) => {
  projectName = randomstring.generate({ length: 10, charset: "alphabetic" });

  const projectFabricator = new ProjectFabricator(request, user);
  await projectFabricator.create(projectName);
  smocker = projectFabricator.smocker;

  const signInPage = new SignInPage(page);
  await signInPage.visit();
  await signInPage.signIn(user.name, user.password);

  projectsPage = new ProjectsPage(page);
  await projectsPage.searchForProject(projectName);
  await projectsPage.expectProjectToBeVisible(projectName);
});

test.afterEach(async () => {
  await smocker.dispose();
});

test("disables dependency update job", async ({ request }) => {
  await projectsPage.setUpdateJobState(projectName, "bundler", "/", false);

  const response = await request.get(`/api/v2/projects/${projectName}/update_jobs`, {
    headers: user.basicAuthHeader
  });
  const responseJson = await response.json();

  expect(responseJson[0]["enabled"]).toBe(false);
});

// TODO: Implement self signed certificate support and repo setup
test.skip("executes dependency update job", async ({ request }) => {
  test.slow(); // this spec is executing dependency update job which also requires pulling docker image

  smocker.add(mocks.updateDependencies(projectName));

  await projectsPage.triggerUpdateJob(projectName, "bundler", "/");

  let updateJob;

  await expect
    .poll(
      async () => {
        const response = await request.get(`/api/v2/projects/${projectName}/update_jobs`, {
          headers: user.basicAuthHeader
        });
        const responseJson = await response.json();
        updateJob = responseJson[0];

        return updateJob["last_finished"];
      },
      {
        message: "Expected update job to be executed",
        timeout: 60_000,
        intervals: [5_000]
      }
    )
    .not.toBe(null);
  expect(updateJob["last_failures"]).toBe("");

  await smocker.verify();
});
