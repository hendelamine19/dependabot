#!/bin/bash

set -e

if [ "${WITH_SERVICE}" == "true" ]; then
  . "$(dirname "$0")/setup.sh" "$PWD"
  . "${APP_START_SCRIPT}" "tail"
else
  bundle exec solargraph socket
fi
