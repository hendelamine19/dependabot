# frozen_string_literal: true

class BackgroundMigrations < Mongoid::Migration
  def self.up
    DataMigration.where(version: "20230714093026").or(version: "20230714133339").destroy_all
  end

  def self.down; end
end
