# frozen_string_literal: true

class ProjectsController < ApplicationController
  before_action :authenticate_user!

  # Render index projects page
  #
  # @return [void]
  def index; end

  # Render projects table
  #
  # @return [void]
  def table
    query = params[:active] == "true" ? { configuration: { "$ne" => nil } } : { configuration: nil }
    projects = if params[:project_name].blank?
                 Project.where(query).order(name: :asc).page(params[:page])
               else
                 Project.where("$text" => { "$search" => params[:project_name] })
                        .where(query).order(name: :asc)
                        .page(params[:page])
               end
    return render partial: "empty_content" if projects.empty?

    render partial: "projects_table", locals: { projects: projects }
  rescue StandardError => e
    render partial: "shared/alert", locals: { alert_type: "danger", message: "Oops, something went wrong: #{e}" }
  end

  # Add new project
  #
  # @return [void]
  def create
    project_name = params.require(:project_name)
    access_token = params[:access_token].presence

    existing_project = find_project(name: project_name)
    return notify(:warning, "Project already exists!") if existing_project

    project = Dependabot::Projects::Creator.call(params.require(:project_name), access_token: access_token)
    return notify(:warning, "Project without configuration added!") unless project.configuration

    Cron::JobSync.call(project)
    notify(:success, "Project added successfully!")
  rescue StandardError => e
    notify(:error, e.message)
  end

  # Sync existing project
  #
  # @return [void]
  def update
    project = find_project(id: params.require(:id))

    updated_project = Dependabot::Projects::Creator.call(project.name, access_token: project.gitlab_access_token)
    Cron::JobSync.call(updated_project)

    notify(:success, "Project synced successfully!")
  rescue StandardError => e
    notify(:error, e.message)
  end

  # Delete existing project
  #
  # @return [void]
  def destroy
    project = find_project(id: params.require(:id))
    Dependabot::Projects::Remover.call(project)

    notify(:success, "Project deleted successfully!")
  rescue StandardError => e
    notify(:error, e.message)
  end

  private

  # Add notification popup
  #
  # @param [Symbol] type
  # @param [String] message
  # @return [void]
  def notify(type, message)
    send("notify_#{type}", message) { redirect_to(projects_path) }
  end
end
