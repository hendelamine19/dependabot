# frozen_string_literal: true

module Update
  class RunsController < ApplicationController
    before_action :authenticate_user!

    def show
      @project = Project.find_by(id: params.require(:project_id).to_i)
      @run = Update::Run.find(params.require(:id))
    end
  end
end
