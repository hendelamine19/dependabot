# frozen_string_literal: true

module Dependabot
  module Options
    # Commit message related options
    #
    class CommitMessage < OptionsBase
      # Transform commit message options
      #
      # @return [Hash]
      def transform
        message_options = opts[:"commit-message"]
        return {} unless message_options

        {
          commit_message_options: {
            prefix: message_options[:prefix],
            prefix_development: message_options[:"prefix-development"],
            include_scope: message_options[:include],
            trailers: message_options[:trailers]&.reduce({}, :merge),
            trailers_security: message_options[:"trailers-security"]&.reduce({}, :merge),
            trailers_development: message_options[:"trailers-development"]&.reduce({}, :merge)
          }.compact
        }
      end
    end
  end
end
