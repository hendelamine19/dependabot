# frozen_string_literal: true

module Mr
  # Recreate merge request
  #
  class RecreateJob < ApplicationJob
    include MrJobConcern
    using Rainbow

    queue_as :high

    sidekiq_options retry: false

    # Perform merge request recreation
    #
    # @param [String] project_name
    # @param [Number] mr_iid
    # @param [String] discussion_id
    # @return [void]
    def perform(project_name, mr_iid, discussion_id = nil)
      @project_name = project_name
      @mr_iid = mr_iid
      @discussion_id = discussion_id

      run_within_context({ job: "mr-update", project: project_name, mr: "!#{mr_iid}" }) do
        init_gitlab

        recreate_mr
      end
    end

    private

    attr_reader :discussion_id

    # Recreate merge request
    #
    # @return [void]
    def recreate_mr
      log(:info, "Recreating merge request #{web_url.bright}".strip)
      reply_status(<<~MSG.strip)
        :warning: `dependabot-gitlab` is recreating merge request. All changes will be overwritten! :warning:
      MSG

      run_task_in_container(package_ecosystem, "recreate_mr", [project_name, mr_iid, discussion_id])
    rescue StandardError => e
      reply_status(":x: `dependabot-gitlab` failed to recreate merge request. :x:\n\n```\n#{e}\n```")
      raise(e)
    end

    # Add reply when recreate performed via comment command
    #
    # @param [String] message
    # @return [void]
    def reply_status(message)
      return unless discussion_id

      Gitlab::MergeRequest::DiscussionReplier.call(
        project_name: project_name,
        mr_iid: mr_iid,
        discussion_id: discussion_id,
        note: message
      )
    end
  end
end
