# frozen_string_literal: true

module Mr
  # :reek:InstanceVariableAssumption

  # Update single merge request based on received webhook event
  #
  class UpdateJob < ApplicationJob
    include MrJobConcern
    using Rainbow

    SKIP_DETAILS = {
      closed: { level: :warn, reason: "Merge request is not in opened state!" },
      assignee_not_matched: { level: :warn, reason: "Merge request assignee doesn't match configured one!" },
      strategy: { level: :info, reason: "Rebase strategy is 'auto' and no conflicts present" }
    }.freeze

    queue_as :high

    sidekiq_options retry: false

    # Perform merge request rebase
    #
    # @param [String] project_name
    # @param [Number] mr_iid
    # @return [void]
    def perform(project_name, mr_iid, ecosystem_update: false)
      @project_name = project_name
      @mr_iid = mr_iid
      @ecosystem_update = ecosystem_update

      run_within_context({ job: "mr-update", project: project_name, mr: "!#{mr_iid}" }) do
        init_gitlab

        update_mr
      end
    end

    private

    attr_reader :ecosystem_update

    delegate :configuration, to: :project, prefix: true

    # Run merge request update
    #
    # @return [void]
    def update_mr
      log(:info, "Running update for merge request #{web_url.bright}".strip)
      if skip_details
        return log(skip_details[:level], "  skipped merge request #{loggable_mr_iid}. #{skip_details[:reason]}")
      end

      conflicts? ? recreate_mr : rebase_mr
    end

    # Config entry for specific package ecosystem and directory
    #
    # @return [Hash]
    def config_entry
      @config_entry ||= configuration_entry(project_configuration, package_ecosystem, directory)
    end

    # Auto-rebase assignee option
    #
    # @return [Boolean]
    def rebase_assignee
      @rebase_assignee ||= config_entry.dig(:rebase_strategy, :with_assignee)
    end

    # Merge request assignee
    #
    # @return [String]
    def mr_assignee
      @mr_assignee ||= gitlab_mr.to_h.dig("assignee", "username")
    end

    # Gitlab merge request
    #
    # @return [Gitlab::ObjectifiedHash]
    def gitlab_mr
      @gitlab_mr ||= gitlab.merge_request(project_name, mr_iid)
    end

    # Skipping details based on conditions
    #
    # @return [Hash]
    def skip_details # rubocop:disable Metrics/CyclomaticComplexity
      return @details if @details
      return @details = SKIP_DETAILS[:closed] if gitlab_mr.state != "opened"
      return @details = SKIP_DETAILS[:assignee_not_matched] if rebase_assignee && rebase_assignee != mr_assignee

      @details = SKIP_DETAILS[:strategy] if ecosystem_update && (rebase_auto? && !conflicts?)
    end

    # Emphasized loggable mr iid
    #
    # @return [String]
    def loggable_mr_iid
      "!#{mr_iid}".bright
    end

    # Is mr updateable
    # * mr in open state
    # * mr has correct assignee when configured and auto-rebase action performed
    #
    # @return [Boolean]
    def updateable?
      gitlab_mr.state == "opened" && (!rebase_assignee || rebase_assignee == mr_assignee)
    end

    def conflicts?
      gitlab_mr["has_conflicts"]
    end

    # Rebase strategy is set to auto
    #
    # @return [Boolean]
    def rebase_auto?
      config_entry.dig(:rebase_strategy, :strategy) == Dependabot::Options::Rebase::AUTO
    end

    # Rebase merge request
    #
    # @return [void]
    def rebase_mr
      gitlab.rebase_merge_request(project_name, mr_iid)

      log(:info, "  rebased merge request #{loggable_mr_iid}.")
    end

    # Recreate merge request
    #
    # @return [void]
    def recreate_mr
      run_task_in_container(package_ecosystem, "recreate_mr", [project_name, mr_iid])
    end
  end
end
