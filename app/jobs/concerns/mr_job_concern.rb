# frozen_string_literal: true

module MrJobConcern
  extend ActiveSupport::Concern

  included do
    include RunnerHelperConcern

    delegate :package_ecosystem, :directory, :web_url, to: :mr

    attr_reader :project_name, :mr_iid
  end

  private

  # Initialize gitlab client with project specific token
  #
  # @return [void]
  def init_gitlab
    token = project.gitlab_access_token
    return unless token

    log(:debug, "Initializing gitlab client with project specific token")
    Gitlab::ClientWithRetry.client_access_token = token
  end

  # Persisted project
  #
  # @return [Project]
  def project
    @project ||= Project.find_by(name: project_name)
  end

  # Find merge request
  #
  # @return [MergeRequest]
  def mr
    @mr ||= project.merge_requests.find_by(iid: mr_iid)
  end
end
