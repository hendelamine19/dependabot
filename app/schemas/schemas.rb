# frozen_string_literal: true

module Schemas
  class ValidationError < StandardError
    UUID_REGEX = / in schema \S+/

    def initialize(msg)
      @msg = msg
    end

    def message
      msg.gsub(UUID_REGEX, "")
    end

    def to_s
      message
    end

    private

    attr_reader :msg
  end

  class ValidationResult
    def initialize(errors)
      @errors = errors
    end

    def success?
      errors.empty?
    end

    def failure?
      errors.any?
    end

    def error_message(separator = "\n")
      return "" if success?

      errors.map { |error| ValidationError.new(error) }.join(separator)
    end

    private

    attr_reader :errors
  end

  class << self
    # Main configuration file schema
    #
    # @return [Hash]
    def configuration_schema
      @configuration_schema ||= JSON.parse(
        File.read(Rails.root.join("config/schemas/configuration.schema.json").to_s)
      )
    end

    # Schema for hex type
    #
    # @return [Hash]
    def registries_hex_schema
      @registries_hex_schema ||= JSON.parse(
        File.read(Rails.root.join("config/schemas/registries/hex.schema.json").to_s)
      )
    end

    # Schema for configuration with key
    #
    # @return [Hash]
    def registries_key_schema
      @registries_key_schema ||= JSON.parse(
        File.read(Rails.root.join("config/schemas/registries/key.schema.json").to_s)
      )
    end

    # Schema for configuration without auth
    #
    # @return [Hash]
    def registries_no_auth_schema
      @registries_no_auth_schema ||= JSON.parse(
        File.read(Rails.root.join("config/schemas/registries/no-auth.schema.json").to_s)
      )
    end

    # Schema for configuration with password and username
    #
    # @return [Hash]
    def registries_password_schema
      @registries_password_schema ||= JSON.parse(
        File.read(Rails.root.join("config/schemas/registries/password.schema.json").to_s)
      )
    end

    # Schema for configuration with token
    #
    # @return [Hash]
    def registries_token_schema
      @registries_token_schema ||= JSON.parse(
        File.read(Rails.root.join("config/schemas/registries/token.schema.json").to_s)
      )
    end

    # Validator for hours input in schedule
    #
    # @return [void]
    def hours_validator
      lambda do |hours|
        pattern = "^\\d{1,2}-\\d{1,2}$"

        if !Regexp.new(pattern).match?(hours)
          raise JSON::Schema::CustomFormatError,
                "has invalid format, must match pattern '#{pattern}'"
        elsif hours.split("-").yield_self { |num| num[0].to_i >= num[1].to_i }
          raise JSON::Schema::CustomFormatError,
                "has invalid format, first number in range must be smaller or equal than second"
        elsif hours.split("-").any? { |num| num.to_i <= 0 && num.to_i >= 23 }
          raise JSON::Schema::CustomFormatError,
                "has invalid format, hours must be between 0 and 23"
        end
      end
    end
  end
end

JSON::Validator.register_format_validator("hours", Schemas.hours_validator)
