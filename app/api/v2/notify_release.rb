# frozen_string_literal: true

module V2
  class NotifyRelease < Grape::API
    include AuthenticationSetup

    desc "Update dependency" do
      detail "Trigger specific dependency update for given package ecosystem across projects"
      success message: "Successfully triggered dependency update", examples: { dependency_update_triggered: true }
    end
    params do
      requires :dependency_name, type: String, desc: "Dependency name"
      requires :package_ecosystem, type: String, desc: "Package ecosystem"
      optional :project_name, type: String, desc: "Specific project to trigger update for"
      optional :ignore_rules, type: Boolean, desc: "Skip allow/ignore rules check when triggering update"
    end
    post :notify_release do
      NotifyReleaseJob.perform_later(
        params[:dependency_name],
        params[:package_ecosystem],
        params[:project_name],
        params[:ignore_rules]
      )

      present({ dependency_update_triggered: true })
    end
  end
end
