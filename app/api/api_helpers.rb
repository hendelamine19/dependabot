# frozen_string_literal: true

module APIHelpers
  include ApplicationHelper

  def project(id: params[:id])
    args = id.match?(/\d+/) ? { id: id.to_i } : { name: id }
    find_project(raise_on_missing: true, **args)
  end
end
