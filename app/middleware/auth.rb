# frozen_string_literal: true

# Auth middleware for mounted rack apps
#
class Auth
  extend AuthHelper

  def initialize(app)
    @app = app
  end

  def call(env)
    return @app.call(env) if AppConfig.anonymous_access

    request = Rack::Request.new(env)
    return @app.call(env) if auth_by_id?(request) || basic_auth?(request)

    unauthorized
  rescue AuthHelper::AuthError, Mongoid::Errors::DocumentNotFound
    unauthorized
  end

  def auth_by_id?(request)
    user_id = request.session[:current_user_id]
    user_id && User.find_by(id: user_id)
  end

  def basic_auth?(request)
    authorization = request.get_header("HTTP_AUTHORIZATION")
    authorization && AuthHelper.authenticate_basic_auth_base64!(authorization)
  end

  def unauthorized
    [401, { "Content-Type" => "text/plain" }, ["Unauthorized"]]
  end
end
