# frozen_string_literal: true

module Dependabot
  module OptionsHelper
    private

    # Transform key names for options related to update allow/ignore rule options
    #
    # @param [Array] opts
    # @return [Array]
    def transform_rule_options(opts)
      opts&.map do |opt|
        {
          dependency_name: opt[:"dependency-name"],
          dependency_type: opt[:"dependency-type"],
          versions: opt[:versions],
          update_types: opt[:"update-types"]
        }.compact
      end
    end
  end
end
