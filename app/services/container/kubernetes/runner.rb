# frozen_string_literal: true

require "securerandom"

module Container
  module Kubernetes
    class Runner < Base
      def initialize(...)
        @container_startup_timeout = UpdaterConfig.updater_container_startup_timeout

        super
      end

      def call
        super

        run_updater
        wait_for_completion
      ensure
        print_log
        delete_pod
      end

      private

      attr_reader :container_startup_timeout

      # Kubernetes client
      #
      # @return [Kubeclient::Client]
      def client
        @client ||= Client.new
      end

      # Updater pod name
      #
      # @return [String]
      def pod_name
        @pod_name ||= resource_hash.dig(:metadata, :name)
      end

      # Deployment namespace
      #
      # @return [String]
      def namespace
        @namespace ||= resource_hash.dig(:metadata, :namespace)
      end

      # Pod resource definition hash
      #
      # @return [Hash]
      def resource_hash
        @resource_hash ||= YAML.safe_load(pod_yml, symbolize_names: true)
      end

      # Pod template yml
      #
      # @return [String]
      def pod_yml
        @pod_yml ||= format(
          File.read(UpdaterConfig.updater_template_path),
          package_ecosystem: package_ecosystem,
          rake_task: rake_task,
          name_sha: unique_name_postfix
        )
      end

      # Run updater
      #
      # @return [void]
      def run_updater
        log(:info, "Creating updater pod '#{pod_name}'")
        pod = client.create_pod(Kubeclient::Resource.new(resource_hash))

        log(:info, "Waiting for updater container to start")
        wait_for_startup(pod)
      end

      # Get updater pod
      #
      # @return [Kubeclient::Resource]
      def updater_pod
        client.get_pod(pod_name, namespace)
      end

      # Wait for updater container to start
      #
      # @param [Kubeclient::Resource] pod
      # @return [void]
      def wait_for_startup(pod)
        tries = 1

        while pod.status.phase == "Pending"
          if tries > container_startup_timeout
            raise(Failure, "Updater container failed to start: #{pod.status.containerStatuses}")
          end

          log(:debug, "  updater container is still pending, waiting...")
          tries += 1
          pod = updater_pod
          sleep(1)
        end
        log(:info, "Updater container started successfully")
      end

      # Print updater container log
      #
      # @return [void]
      def print_log
        log(:info, "Fetching updater container log")
        puts client.get_pod_log(pod_name, namespace) # rubocop:disable Rails/Output
      rescue StandardError => e
        log_error(e, message_prefix: "Failed to fetch updater container log")
      end

      # Wait for pod finish status
      #
      # @return [void]
      def wait_for_completion
        phase = updater_pod.status.phase

        log(:info, "Waiting for updater pod completion")
        while phase == "Running"
          log(:debug, "  pod is still running, waiting...")
          sleep(5)
          phase = updater_pod.status.phase
        end
        return if phase == "Succeeded"

        raise(Failure, <<~ERR)
          Updater pod did not finish successfully.
          Expected pod phase to be 'Succeeded', got: '#{phase}'
        ERR
      end

      # Delete updater pod
      #
      # @return [void]
      def delete_pod
        unless UpdaterConfig.delete_updater_container?
          log(:info, "Skipping updater pod deletion due to container delete option set to 'false'")
          return
        end

        log(:info, "Deleting pod '#{pod_name}'")
        client.delete_pod(pod_name, namespace)
      rescue StandardError => e
        log_error(e, message_prefix: "Failed to delete updater pod '#{pod_name}'")
      end
    end
  end
end
