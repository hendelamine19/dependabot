# frozen_string_literal: true

module ServiceHelpersConcern
  # Initialize gitlab client with project specific token
  #
  # @param [Project] project
  # @return [void]
  def init_gitlab(project)
    token = project.gitlab_access_token
    return unless token

    log(:debug, "Initializing gitlab client with project specific token")
    Gitlab::ClientWithRetry.client_access_token = token
  end
end
