# frozen_string_literal: true

module Github
  module Vulnerabilities
    # Local store for package ecosystem vulnerabilities
    #
    class LocalStore < ApplicationService
      using Rainbow

      def initialize(package_ecosystem)
        @package_ecosystem = package_ecosystem
      end

      def call
        RequestStore.fetch(:"#{package_ecosystem}-vulnerabilities") { fetch_vulnerabilities }
      end

      private

      attr_reader :package_ecosystem

      # Active vulnerabilities
      #
      # @return [Array<Vulnerability>]
      def fetch_vulnerabilities
        log(:info, "Fetching vulnerability data from GitHub advisory database")
        return [] unless supported?

        Github::Vulnerabilities::Fetcher
          .call(package_ecosystem)
          .select { |vulnerability| vulnerability.withdrawn_at.nil? }
          .tap { |vulnerabilities| log(:info, "  fetched #{vulnerabilities.size.to_s.bright} vulnerabilities") }
      rescue StandardError => e
        log_error(e, message_prefix: "Failed to fetch vulnerabilities, skipping")
        []
      end

      # Fetching vulnerabilities supported?
      #
      # @return [Boolean]
      def supported?
        unless Fetcher::PACKAGE_ECOSYSTEMS.key?(package_ecosystem)
          log(:info, "  ecosystem #{package_ecosystem.bright} not supported, skipping")
          return false
        end

        if CredentialsConfig.github_access_token.blank?
          log(:warn, "  missing GitHub access token, skipping")
          return false
        end

        true
      end
    end
  end
end
