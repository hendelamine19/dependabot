# frozen_string_literal: true

# :reek:MissingSafeMethod

module Update
  # Dependency update run
  #
  # @!attribute created_at
  #   @return [DateTime]
  # @!attribute updated_at
  #   @return [DateTime]
  # @!attribute job
  #   @return [Update::Job]
  # @!attribute log_entries
  #   @return [Array<LogEntry>]
  # @!attribute failures
  #   @return [Array<Failure>]
  # @!attribute failed
  #   @return [Boolean]
  # @!attribute status
  #   @return [String]
  class Run
    include Mongoid::Document
    include Mongoid::Timestamps

    SUCCESS = "success"
    FAILED = "failed"
    WARNING = "warning"
    RUNNING = "running"

    field :finished_at, type: DateTime
    field :status, type: String

    has_many :log_entries, class_name: "Update::LogEntry", dependent: :destroy
    has_many :failures, class_name: "Update::Failure", dependent: :destroy

    belongs_to :job, class_name: "Update::Job"

    # Persist log entries
    #
    # @param [Array<Hash>] logs
    # @return [void]
    def save_log_entries!(logs)
      log_entries.create!(logs.map { |entry| { **entry, run: self } })
    end

    # Persist job errors
    #
    # @param [Array<Hash>] errors
    # @return [void]
    def save_errors!(errors)
      failures.create!(errors.map { |entry| { **entry, run: self } })
    end

    class Entity < Grape::Entity
      format_with(:utc) { |dt| dt&.utc }

      expose :id, documentation: { type: String, desc: "Run id" } do |run, _options|
        run.id.to_s
      end
      expose :status, documentation: { type: String, desc: "Run status" }
      expose :created_at, format_with: :utc, documentation: { type: DateTime, desc: "Run created at" }
      expose :finished_at, format_with: :utc, documentation: { type: DateTime, desc: "Run finished at" }

      # Example response hash
      #
      # @return [Hash]
      def self.example_response
        {
          id: "5d5b5c5d5e5f5a5b5c5d5e5f",
          created_at: Time.zone.now.utc,
          finished_at: Time.zone.now.utc,
          status: "success"
        }
      end
    end
  end
end
