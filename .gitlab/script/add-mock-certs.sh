#!/bin/bash

set -e

source "$(dirname "$0")/utils.sh"

log_with_header "Add mock certs"
curl -sSL https://gitlab.com/dependabot-gitlab/ci-images/-/raw/main/scripts/gogs/certs/cert.pem > /usr/local/share/ca-certificates/smocker.crt
update-ca-certificates
log_success "success!"
