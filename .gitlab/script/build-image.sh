#/bin/bash

# Build script for image building on CI
#
# Parameter 'updater_image_separator' is used to allow pushing to GitLab container registry which uses / as separator

set -e

source "$(dirname "$0")/utils.sh"

app_image_name=$1
current_tag=$2
latest_tag=$3
image_type=$4
arch="${ARCH:-amd64}"

base_image="registry.gitlab.com/dependabot-gitlab/core-images/$image_type:$(dependabot_version)"

if [ "$image_type" == "core" ]; then
  dockerfile="Dockerfile.core"
  image="$app_image_name"
else
  dockerfile="Dockerfile.ecosystem"
  image="${app_image_name}/${image_type}"
fi

images="${image}:${current_tag}-${arch},${image}:${latest_tag}-${arch}"

log_with_header "Building image '$image:$current_tag'"
docker buildx build \
  --file "$dockerfile" \
  --platform="linux/${arch}" \
  --provenance "false" \
  --build-arg COMMIT_SHA="$CI_COMMIT_SHA" \
  --build-arg PROJECT_URL="$CI_PROJECT_URL" \
  --build-arg VERSION="${CI_COMMIT_TAG:-$current_tag}" \
  --build-arg BASE_IMAGE="$base_image" \
  --cache-from type=registry,ref="$image:latest-${arch}" \
  --cache-from type=registry,ref="$image:$latest_tag-${arch}" \
  --cache-to type=inline \
  --output type=image,\"name="$images"\",push=true \
  .
